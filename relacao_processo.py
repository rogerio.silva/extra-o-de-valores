from func_ext_sent import ext_senteca
from func_ext_sent import export
import pandas as pd
import sqlalchemy
import urllib

connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=sql-convex.database.windows.net;DATABASE=flex;UID=convex;PWD=SF0xvcUPGrt81; connect_timeout=10000'
connection_string = urllib.parse.quote_plus(connection_string)
connection_string = 'mssql+pyodbc:///?odbc_connect=%s' % connection_string
engine = sqlalchemy.create_engine(connection_string)
e = engine.connect()


query = """with a as (
select mv.IDPROCESSO, mv.IDMOVIMENTO, SUBSTRING(deteormovimento, (len(deteormovimento) - CHARINDEX(REVERSE('Julgo'), REVERSE(deteormovimento)) - 4), len(deteormovimento)) as 'DETEORMOVIMENTO' 
from pg_movimentos mv  
join hierarquiamovimentos h on mv.detipomovimento = h.detipomovimento and h.decategoria = 'Julgamento' 
where deteormovimento like '%Julgo%' and deteormovimento like '%R$%'
)
select * from a where deteormovimento like '%R$%'"""


data = pd.read_sql(query,e)


data['LIDO'] = data.apply(lambda row: ext_senteca(row.IDPROCESSO, row.IDMOVIMENTO, row.DETEORMOVIMENTO), axis =1)

data.to_sql('DATA_FILTER',e,if_exists='replace', index=False)

a = 'IMPORTAR'

f = export(a)

print(f)
