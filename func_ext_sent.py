import nltk
from nltk.tokenize import sent_tokenize
nltk.download('machado')
import spacy
from nltk import word_tokenize
import pandas as pd
import sqlalchemy
import urllib
import re


#String de conexão banco de dados base Flex
connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=sql-convex.database.windows.net;DATABASE=flex;UID=convex;PWD=SF0xvcUPGrt81'
connection_string = urllib.parse.quote_plus(connection_string)
connection_string = 'mssql+pyodbc:///?odbc_connect=%s' % connection_string
engine = sqlalchemy.create_engine(connection_string)
e = engine.connect()


#String de conexão com o banco local
# connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=TESTE;UID=SA;PWD=55262408Ro'
# connection_string = urllib.parse.quote_plus(connection_string)
# connection_string = 'mssql+pyodbc:///?odbc_connect=%s' % connection_string
# engine = sqlalchemy.create_engine(connection_string)
# e = engine.connect()


#dicionario de classificação
dicionario = {}
dicionario['DANO'] = ['DANO MORAL', 'DANO MATERIAL', 'DANO']
dicionario['MULTA'] = ['MULTA']
dicionario['EXECUÇÃO DE TITULO'] = ['NO IMPORTE', 'TÍTULO EXECUTIVO']
dicionario['REMESSA'] = ['REMESSA','TAXA JUD', 'PORTE DE RETORNO']
dicionario['RESTITUIÇÃO'] = ['RESTITUIR', 'RESTITUIÇÃO', 'RESTITUIU','SALDO DEVEDOR', 'AÇÃO MONITÓRIA', 'PAGAR AO AUTOR']
dicionario['PREPARO'] = ['PREPARO']
dicionario['HONORÁRIOS'] = ['HONORÁRIOS']
dicionario['CUSTAS EM ABERTO'] = ['CUSTAS EM ABERTO', 'DESPESAS POSTAIS DEVIDAS']
dicionario['TAXAS JUDICIAIS'] = ['TAXAS JUDICIÁ', 'TAXA DE SERVIÇOS DE TERCEIROS', 'UFESP']

#lista de splits
altera = ['PORTE', 'TAXA', '/ ', 'DESPESAS POSTAIS']


#Lista de Replaces - primeiro elemento, elemento a ser substituido, segundo elemento a substituição
replaces = [('CÓD.','CÓD'), ('R$.', 'R$'),('R$;','R$'),('ART.','ART '), ('FLS.', 'FLS'),( '?',' '),('FL.','FL')]



lista = list()

tabela = pd.DataFrame(columns=['IDPROCESSO','IDMOVIMENTO','TIPO','VALOR','TRECHO'])

def return_table(pos, t, IDPROCESSO, IDMOVIMENTO,tpsent_= None):
    global tabela
    tpsent = tpsent_ if tpsent_ is not None else pos
    tok = word_tokenize(t)
    posi = tok.index('$')
    if len(tok) > (posi+1):
        text = tok[posi+1] #text = tok[posi + 1] if len(tok) < (posi+1) else 00,00
    else:
        text = 00,00
    valor = (text).__str__()
    valor = re.sub('([^0-9 \, \.])', '', valor)
    tabela = tabela.append(
        {
            'IDPROCESSO': IDPROCESSO,
            'IDMOVIMENTO': IDMOVIMENTO,
            'TIPO': tpsent,
            'VALOR': valor,
            'TRECHO': t,

        }, ignore_index=True
    )

def ext_classific(item, IDPROCESSO,IDMOVIMENTO):
    t = item
    encontrou = False
    for pos in dicionario.keys():

        if any([words in t for words in dicionario[pos]]):
            return_table(pos, t, IDPROCESSO, IDMOVIMENTO)
            encontrou=True
    if not encontrou:
        return_table(pos, t, IDPROCESSO, IDMOVIMENTO,'NÃO CLASSIFICADO')




def ext_senteca(IDPROCESSO,IDMOVIMENTO,DETEORMOVIMENTO):

    lista = list()
    global tabela
    IDPROCESSO = IDPROCESSO
    IDMOVIMENTO = IDMOVIMENTO

    DETEORMOVIMENTO = DETEORMOVIMENTO.upper()

#percorre a lista de replaces
    for i in replaces:
        j = i[0]
        f = i[1]
        DETEORMOVIMENTO = DETEORMOVIMENTO.replace(j,f)


# percorre a lista de splits
    for f in altera:
        if f in DETEORMOVIMENTO:
            new = '; '+f
            DETEORMOVIMENTO = DETEORMOVIMENTO.replace(f,new)

# Splita onde existir a exceção
    DETEORMOVIMENTO = DETEORMOVIMENTO.split(';')

#Inicia a Iteração
    for f in DETEORMOVIMENTO:
        f = sent_tokenize(f)
        for i in f:
            if '$' in i and 'SOB PENA' not in i: #como estava      ------ if 'R$' in i:
                lista.append(i)
    for t in lista:

        ext_classific(t, IDPROCESSO,IDMOVIMENTO)
    #tabela = pd.DataFrame(columns=['IDPROCESSO','IDMOVIMENTO','TIPO','VALOR','TRECHO'])
    s = lista.__str__()
    return s



def export(S):
    # #teste
    # connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=TESTE;UID=SA;PWD=55262408Ro'
    # connection_string = urllib.parse.quote_plus(connection_string)
    # connection_string = 'mssql+pyodbc:///?odbc_connect=%s' % connection_string
    # engine = sqlalchemy.create_engine(connection_string)
    # e = engine.connect()

    print('ROTINA DE EXPORTAÇÃO INICIADA!')
    print('Linhas a sendo Inseridas no Banco')
    tabela.to_csv('EXTSENTENCA.csv', sep='#')
    print(len(tabela))
    tabela.to_sql('EXT_VALOR_SENTENCA', e, if_exists='replace', index=False)
    f = tabela.count()
    return f